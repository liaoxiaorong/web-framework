#!/usr/bin/env python
#
# Copyright 2013 Xiaorong Liao
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.


import re
import os
import sys
import traceback
import urllib

import template

class RequestHandler(object):
    def __init__(self):
        self._headers = {
            "Content-type": "text/html; charset=utf-8",
            }
        self._status_code = "200 OK"

    def header(self, headers):
        for k,v in headers.items():
            self._headers[k] = v

    def render(self, filepath, **kwargs):
        dirname, filename = os.path.split(os.path.abspath(filepath))

        frame = sys._getframe(0)
        web_file = frame.f_code.co_filename
        while frame.f_code.co_filename == web_file:
            frame = frame.f_back
        dirname = os.path.dirname(frame.f_code.co_filename) + dirname
            
        loader = template.Loader(dirname)
        namespace = dict()
        namespace.update(kwargs)
        return loader.load(filename).generate(**namespace)

    def get(self, *args, **kwargs):
        raise HTTPError(405)

    
    def post(self, *args, **kwargs):
        raise HTTPError(405)

    def redirect(self, url):
        self._status_code = "302"
        self._headers['Location'] = urllib.urlencode(url)

class WSGIApplication(object):
    def __init__(self, urls=None):
        self.urls = urls

        for pattern, handler in self.urls:
            self.status = handler._status_code
            self._headers = handler. _headers
            
    def __call__(self, environ, start_response):
        self.environ = environ
        self.start = start_response

        try:
            x = self.delegate()
            headers = list(handler._headers.get_all())
            self.start(self.status,
                       (urllib.urlencode(k), urllib.urlencode(v)) for (k,v) in self._headers)
        except:
            header = [("Content-type", "text/plain; charset=utf-8")]
            self.start("500 Internal Error", header)
            x = "Internal Error:\n\n" + traceback.format_exc()

        if isinstance(x, str):
            return iter([x])
        else:
            return iter(x)

    def delegate(self):
        path = self.environ['PATH_INFO']
        method = self.environ['REQUEST_METHOD']

        for pattern, handler in self.urls:
            m = re.match("^" + pattern + "$", path)
            if m:
                args = m.groups()
                func = method.lower()
                if hasattr(handler, func):
                    hInstance = handler()
                    func = getattr(hInstance, func)
                    return func(*args) 
        return self.notfound()

    
    def notfound(self):
        return "404 Not Found"
