weblib
======

a simple wsgi web-framework.(Is this another web framework?)

I recommond ues apache+mod_wsgi as you web server, and below is a simple apache configure file.

    <VirtualHost *:80>

    ServerAdmin webmaster@example.com

    DocumentRoot /var/www/py
    <Directory /var/www/>
    	Options Indexes FollowSymLinks MultiViews
    	AllowOverride None
    	Order allow,deny
    	allow from all
    </Directory>
          
    WSGIScriptAlias / /var/www/py/application.wsgi

    Alias /resource /var/www/py/resource

    <Directory /var/www/py/resource>
       # directives to effect the static directory
        Options +Indexes
    </Directory>    

    CustomLog ${APACHE_LOG_DIR}/access.log combined

    </VirtualHost>
    
Get start

    # coding: utf-8
    import sys
    path = '/var/www/py'
    if path not in sys.path:
        sys.path.append(path)

    from weblib import wsgiapp

    class helloHnadler(wsgiapp.RequestHandler):
        def get(self):
            title = "hello wsgiapp"
            return self.render("./view/hello.html", title=title)

    class viewHandler(wsgiapp.RequestHandler):
        def get(self, arg):
            return "whaterver this is " + arg

    application = wsgiapp.WSGIApplication([
        ("/", helloHnadler),
        ("/view/(.*)", viewHandler),
        ])


I write this for demo what is wsgi and web-framework.

I use [tornado](https://github.com/facebook/tornado/) template engine.

And there are a lot features not finish as you see.
