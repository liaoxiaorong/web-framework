# coding: utf-8
import sys
path = '/var/www/py'
if path not in sys.path:
    sys.path.append(path)

import weblib
from weblib import wsgiapp


class helloHnadler(wsgiapp.RequestHandler):
    def get(self):
        title = "hello wsgiapp"
        return self.render("./view/hello.html", title=title)


class viewHandler(wsgiapp.RequestHandler):
    def get(self,arg):
        return "you are amazing " + arg


application = wsgiapp.WSGIApplication([
    ("/", helloHnadler),
    ("/view/(.*)", viewHandler),
    ])


